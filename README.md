Please go to / first, it is very important.

When you log in, you can access /userprofile any time with your cookie.
When user does not exist => 401.
When not logged in, you cannot access /userprofile.

!routes!
/
/login
/userprofile

