const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const userRoute = require('./user');

const PORT = process.env.PORT || 8080

const app = express();

app.set('view engine', 'ejs');

app.use( express.static( "public" ) );
app.use(bodyParser());
app.use(cookieParser());
app.use(userRoute);

app.get('/', function(req, res) {
    res.render('pages/home');
});

app.get('/login', function(req, res) {
    res.render('pages/login');
});

app.get('/userprofile', function(req, res) {
    res.render('pages/userprofile');
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`))